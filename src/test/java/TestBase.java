import entity.UserEntity;
import org.junit.Test;
import top.lingkang.ste.template.Template;
import top.lingkang.ste.template.TemplateContext;
import top.lingkang.ste.template.TemplateLoader;

/**
 * @author lingkang
 * created by 2023/10/30
 */
public class TestBase {
    @Test
    public void test() {
        TemplateLoader loader = new TemplateLoader.ClasspathTemplateLoader();
        Template template = loader.load("/test.st");
        TemplateContext context = new TemplateContext();
        UserEntity user = new UserEntity();
        context.set("user", user);
        System.out.println(template.renderTrim(context));
    }

    @Test
    public void testTxt() {
        TemplateLoader.MapTemplateLoader loader = new TemplateLoader.MapTemplateLoader();
        loader.set("sql", "select e from UserEntity e where 1=1 {{if a>0}} and id='{{a}}'{{end}}");
        TemplateContext context = new TemplateContext();
        context.set("a", 11);
        Template template = loader.load("sql");
        String renderTrim = template.renderTrim(context);
        System.out.println(renderTrim);
    }
}
