## 基本用法

创建一个名为 `helloworld.st` 以下内容的新文件：
```
Hello {{name}}
```
然后，我们可以从类路径上的文件加载模板，设置模板变量的值name，并将模板渲染为字符串，然后将其输出到控制台。
```java
import io.marioslab.basis.template.Template;
import io.marioslab.basis.template.TemplateContext;
import io.marioslab.basis.template.TemplateLoader;
import io.marioslab.basis.template.TemplateLoader.ClasspathTemplateLoader;

public class BasicUsage {
   public static void main (String[] args) {
      TemplateLoader loader = new ClasspathTemplateLoader();
      Template template = loader.load("/helloworld.st");
      TemplateContext context = new TemplateContext();
      context.set("name", "World");
      System.out.println(template.render(context));
   }
}
```
这会产生以下结果：
```
Hello World
```
这说明了您通常会遇到的所有 API 界面。快速浏览一下代码：

* 1、要加载模板，我们需要一个TemplateLoader. Basis-template 为类路径资源、文件和内存模板提供加载器。
* 2、要将变量值传递给模板，我们需要一个TemplateContext. name上下文可以被认为是变量名称（如）及其值（如字符串）的映射"World"。
* 3、为了渲染模板，我们将上下文传递给它。然后，模板评估其中包含的所有表达式，在上下文中查找变量值，最后返回渲染的字符串。

## 文本和代码跨度

模板由文本和代码范围组成。文本范围是任何旧的字符序列。代码跨度是内部的字符序列{{，}}必须符合模板语言语法。文本和代码跨度可以自由混合，例如：
```
Dear {{customer}},

Thank you for purchasing {{license.productName}}. You can find your activation codes below:

{{for index, activationCode in license.activationCodes}}
   {{(index + 1)}}. {{activationCode}}
{{end}}

Please let me know if there is anything else I can help you with!

Kind regards,
Your friendly neighbourhood customer service employee
```
我们可以使用以下代码填充此模板：
```java
public static class License {
   public final String productName;
   public final String[] activationCodes;

   public License (String productName, String[] activationCodes) {
      this.productName = productName;
      this.activationCodes = activationCodes;
   }
}

public static void main (String[] args) {
   TemplateLoader loader = new ClasspathTemplateLoader();
   Template template = loader.load("/textandcodespans.st");
   TemplateContext context = new TemplateContext();
   context.set("customer", "Mr. XiaoMing");
   context.set("license", new License("XiaoMing", new String[] {"3ba34234bcffe", "5bbe77f879000", "dd3ee54324bf3"}));
   System.out.println(template.render(context));
}
```
这将产生输出：
```
Dear Mr. XiaoMing,

Thank you for purchasing XiaoMing. You can find your activation codes below:

   1. 3ba34234bcffe
   2. 5bbe77f879000
   3. dd3ee54324bf3

Please let me know if there is anything else I can help you with!

Kind regards
Your friendly neighbourhood customer service employee
```
当您渲染模板时，模板引擎会评估所有文本和代码范围，并按顺序写出（“发出”）到 aString或。OutputStream文本范围（例如“亲爱的”或“感谢您购买”）以 UTF-8 字符串的形式逐字发出。

代码跨度更加复杂。如果代码范围由类似`{{license.productName}}`or的表达式组成`{{index + 1}}`，则模板引擎首先计算该表达式。如果它产生非 void、非 null 结果，则该结果将转换为 UTF-8 字符串并发出。

像上面这样的一些模板语言结构for由多个混合的代码和文本范围组成。在`for`上面的情况下，`{{for ...}}`和`{{end}}`代码跨度执行非发射代码跨度，因为它们本身不产生值。之间的跨度是文本跨度和表达式代码跨度，生成一个值，因此将被发出（与 for 循环迭代的次数一样多）。

当代码范围是一行中唯一的非空白字符序列，并且代码范围不发出值时，整行将从输出中省略。否则，代码范围将被替换为发出的值，或者如果它不发出值，则从其包含行中完全删除，从 开始到`{{`结束（包含）`}}`。

如果您需要在文本范围中使用{{或，只需使用和对其进行转义即可。只需避开第一个卷曲就足够了。`}}\{\}`

让我们看看我们可以在代码范围内放入什么。

## 文字

基础模板语言支持广泛的文字，类似于 Java 支持的文字。当模板引擎遇到仅由文字组成的代码跨度时，它会根据 Java 的语义将其转换为字符串`String.valueOf()`。如果在表达式求值中使用文字，则`{{1 + 2}}`它将在求值期间采用文字的类型。

布尔文字采用`true` and的形式`false`：
```
{{true}} fake news is {{false}}.
```
数字文字有整数和浮点类型：
```
This is an integer {{123}}, and this is a float {{123.456}}.
```
您可以使用后缀指定数字文字的类型，类似于Java 中的后缀long,float和文字：double
```
A byte {{123b}}.
A short {{123s}}.
An int {{123}}.
A long {{123l}}.
A float {{123f}}.
A double {{123d}}.
```
虽然一般情况下您不会发现很多需要使用后缀的情况，但在调用需要特定类型的函数和方法时它们会派上用场。

注意：模板引擎在计算算术运算时将执行加宽类型强制，例如`{{1b + 2.3}}`. 在这种情况下，byte操作数将首先被扩展为双精度，以匹配`double`操作数。

模板语言还支持字符和字符串文字：
```
The character {{'a'}} is included in the string {{"a-team"}}.
```
字符和字符串文字可能包含常见的转义序列`\n`, `\r`, `\t`。字符`\`、`'`、 和"也必须在字符和字符串文字中进行转义，例如`{{'\\'}}` `{{'\''}}` `{{"\""}}`。

由于 `ste` 是一个 JVM 模板引擎，我们无法避免 10 亿美元的错误`null`，它的字面形式如下：`{{null}}`。如果您想要将方法或函数的返回值与 进行比较`null`，或者必须传递`null`.

最后，基础模板支持映射和数组文字，它们看起来与 JSON 非常相似：
```
{{
    {
        title: "Hello world"
        date: "2018/07/23",
        published: false,
        tags: [ "rant", "JVM", "ponies" ]
    }
}}
```
如您所见，映射和数组文字可以任意嵌套。映射文字将返回 的实例`Map<String, Object>`，列表文字将返回类型的实例`List<Object>`。您在映射文字中指定的键遵循与 `Java` 中的标识符相同的规则。您不能指定带有空格的任意引号字符串作为映射文字的键！

如果您想在模板中定义元数据以便在模板的不同部分中重用，则映射和数组文字会很方便。

## 运营商

模板语言支持大多数 `Java` 运算符。这些运算符的优先级也与 `Java` 中相同。

### 一元运算符
您可以通过一元运算符对数字求反-，例如`{{-234}}`。要对布尔表达式求反，您可以使用!运算符，例如`{{!true}}`。

### 算术运算符
并不完全出乎意料的是，模板引擎支持常见的算术运算符，例如`{{1 + 2 * 3 / 4 % 2}}`。

算术运算符计算两个操作数中较宽类型的值。例如，当添加 a `byte`和 a 时`float`，结果值将具有 type `float`。

与 `Java` 中一样，+运算符也可用于连接字符串与另一个值。在这种情况下，模板引擎将自动将非字符串操作数强制转换为字符串，例如`{{"Lucky number " + 9}}`。

### 比较运算符
您在 `Java` 中了解的所有比较运算符都可供您使用，例如`{{23 < 34}}`, `{{23 <= 34}}`, `{{23 > 34}}`, `{{23 >= 34}}`, `{{ true != false }}`, `{{23 == 34}}`。

注意：相等运算符不会在对象实例上调用 `equals`。相反，它的功能类似于 `Java` 的等效项。

比较运算符的计算结果为布尔值。

### 逻辑运算符
除了一元`!`运算符之外，您还可以使用`&&` and `||`。就像 `Java` 中的运算符一样，它们是短路运算符。如果 的 左侧运算符`&&`计算结果为`false`，则不会计算右侧操作数。如果 的左侧操作数的`||`计算结果为 `false`，则不会计算右侧操作数。

逻辑运算符计算结果为布尔值。

### 三元运算符
三元运算符是语句的简写`if`，其工作方式类似于 `Java`，例如`{{true ? "yes" : "no"}}`。

需要条件来计算布尔值。

> 注意：null不计算布尔值false。这（还）不是 JavaScript。==将or!=运算符与（潜在）值一起使用null。

### 按括号对表达式进行分组
如果您需要更多控制，或者想要明确优先级，您可以使用(和)对表达式进行分组，例如{{(1 + 3) * 4}}。

### 我的按位运算符在哪里？
这些目前尚未实施。您可以将它们实现为函数，或者发送拉取请求以将它们添加到语言中。这很简单，也是一个很棒的练习！

## 上下文和变量

为了使模板有用，我们需要能够向其中注入值。如前所示，这是通过`TemplateContext`在渲染模板时提供 来完成的。

要设置变量值，请调用以下`TemplateContext.set(String, Object)`方法：
```
{{a}} {{b}} {{c}}
```
```
context.set("a", 123).set("b", "Test").set("c", myObject);
System.out.println(template.render(context));
```
```
123 Test @MyObject
```
您可以将变量设置为任何 Java 原语或对象，甚至`null`. 变量名，也称为标识符，遵循与 `Java` 中标识符相同的规则。它们可能以`_`、、`$`或开始`[a-zA-Z]`，然后继续以零个或多个`_`、、或。`$[a-zA-Z][0-9]`

当模板引擎在表达式中遇到变量名时，它会在上下文中查找其值。与其他模板引擎不同，如果未找到该变量名称的值，`RuntimeException`则会抛出 a 。如果您确实需要“可选”变量，您可以通过将变量与 进行比较来检查它们是否存在`null`。

当变量值被计算时，它采用相应的 Java 对象具有的任何类型。例如，an `int`将被视为表达式中的整数，a 将`Map`被视为映射等。模板引擎还将对算术表达式执行扩展类型强制，并以与 `Java` 相同的方式将参数传递给方法和函数。

原始类型的评估很简单。然而，基础模板的真正威力来自于能够访问对象的字段和调用方法。

## 作业
模板语言允许有限形式的分配。您可以在模板中设置上下文变量的值。当您想要存储函数或方法返回值或计算的一些中间结果时，这可能很有用：
```
{{{a = 10}}}
{{a}}
{{a = a + 2}}
{{a}}
```
```
10
12
```
如果变量名已存在于上下文中，则其值将被替换。如果变量名尚不存在，则会创建它。

不支持并且永远不会支持向对象字段、数组或映射分配新值。这将允许从模板内修改 `Java` 端对象，这是一个很大的禁忌。

## 访问字段

当上下文变量指向一个对象时，您可以像在 Java 中一样访问该对象的字段（稍有改动）：
```
Basis-template can access private {{myObject.privateField}}, package private {{myObject.packagePrivateField}}, protected {{myObject.protectedField}}, and public {{myObject.publicField}} fields. It can also access static {{myClass.STATIC_FIELD}} fields.
```
```
public static class MyObject {
   public static String STATIC_FIELD = "I'm static";
   private int privateField = 123;
   boolean packagePrivateField = true;
   protected float protectedField = 123.456f;
   public String publicField = "ello";
}

// ...
context.set("myObject", new MyObject());
context.set("myClass", MyObject.class);
System.out.println(template.render(context));
```
```
Basis-template can access private 123, package private true, protected 123.456, and public ello fields. It can also access static I'm static fields.
```
不同的是，基础模板将超越您的访问修饰符并允许读取私有、包私有和受保护的字段。基础模板中的字段访问比方法调用的性能稍好一些。这个不安全的小功能可以让您提高模板的性能。

> 注意：与其他模板引擎不同，`ste`不解析遵循 `JavaBean` 约定的 `getter` 方法。通过名称访问字段，或调用 `getter`。

## 调用方法

与字段类似，基础模板允许您调用任何对象上的任何方法。
```
{{myObject.add(1, 2)}} {{myObject.add(1f, 2f)}} {{String.format(%010d", 93)}}
public static class MyObject {
   private int add (int a, int b) { return a + b; }
   protected float add (float a, float b) { return a + b; }
   public static String staticMethod () { return "Hello"; }
}

context.set("myObject", new MyObject());
context.set("String", String.class);
System.out.println(template.render(context));
```
```
3 3.0 Hello
```
同样，基础模板允许您完全忽略访问修饰符。

值得注意的一点是，基础模板可以处理重载方法。为此，传递给重载方法的参数类型必须与方法参数类型匹配（这可能需要隐式加宽类型强制）。在上面的示例中，`{{myObject.add(1, 2)}}`将调用该`MyObject.add(int, int)`方法，因为提供的两个参数的类型为int，而 `as{{myObject.add(1f, 2f)}}`将调用该`MyObject.add(float, float)`方法，因为提供的参数的类型为`float`。在类似的情况下将尝试扩大类型强制`{{myObject.add(1b, 1s)}}`。这与`MyObject.add(int, int)`方法相匹配。通过将byte和short参数扩大到`int`.

注意：ste 目前不处理像`String.format()`

## 数组和映射

模板语言还允许您访问数组元素和映射条目：
```
{{myArray[2]}} {{myMap.get("key")}} {{myMap["key"]}}
```
```
context.set("myArray", new int[] { 1, 2, 3 });
Map<String, String> myMap = new HashMap<String, String>();
myMap.put("key", "value");
context.set("myMap", myMap);
template.render(context);
System.out.println(template.render(context));
```
```
3 value value
```
数组元素的访问方式`[index]`与 `Java` 中类似。要访问地图条目，您可以调用`Map.get()`，或使用简写符号`map[key]`。

数组索引和映射键都可以是任意表达式。数组索引必须计算为`ìnt`. 映射键必须评估为映射的键类型。

如果键是没有空格的字符串，则可以使用类似于映射上的成员访问的简写：
```
{{
map = {
    title = "Hello world",
    tags = [ "JVM", "compilers", "ponies" ]
}
title = map.title
firstTag = map.tags[0]
}}
```

## 访问链

与 `Java` 一样，您可以无限嵌套成员、数组元素和映射访问：
```
{{{myObject.aField[12]["key"].someMethod(1, 2).anotherMethod()}}
```
## 功能
`ste` 支持一等公民的功能。为此，您必须使用 `Java 8+ JVM`。但是如何将变量设置为 `Java`“函数”呢？
```
{{cos(3.14)}}
```
```
context.set("cos", (DoubleFunction<Double>)Math::cos);
System.out.println(template.render(context));
```
-0.9999987317275395
```
诀窍是获取方法引用（如`Math::cos`）并将其转换为合适的`FunctionalInterface`。`Java` 编译器会将其转换为一个匿名类实例，其中包含一个名为 的方法apply。当这样的实例被设置为变量的值并且模板引擎遇到该变量作为要调用的函数的名称时，它足够聪明地解析该函数apply​​并反射性地调用它。

通过这个小技巧，您可以构建一个可在我们所有模板中使用的“标准”库，其中包含简短的函数名称，例如 、`trim`等`abs`。

注意：`ste` 不附带任何开箱即用的内置函数。

此功能还允许您将“函数”存储在字段、数组、映射或变量中，本质上使“函数”成为一等公民。
```
This calls Math::abs on the argument: {{array[0](-123)}}
This calls Math::signum on the argument: {{array[1](-7)}}
This calls the function in the field myFunc: {{myObject.myFunc(3)}}
```
```
class MyObject {
IntFunction<Integer> myFunc = v -> return v + 1;
}

// ...
context.set("myObject", new MyObject());
context.set("array", new IntFunction[] {Math::abs, Math::signum});
result = template.render(context);
System.out.println(result);
```
```
This calls Math::abs on the argument: 123
This calls Math::signum on the argument: -1
This calls the function in the field myFunc: 4
```
作为通过模板上下文注入功能接口实例的替代方法，您还可以直接在模板中创建函数。这些称为宏。

## 宏
宏由名称、参数列表和宏体组成。它们本质上是直接在模板中定义的函数：
```
{{macro button(id, text)}}
<input id="{{text}}" value="{{text}}">
{{end}}

<form>
   {{button("send", "Send")}}
   {{button("canel", "Cancel")}}
</form>
```
```
<form>
   <input id="send" value="Send">
   <input id="cancel" value="Cancel">
</form>
```
宏声明（和定义）本身不会发出任何内容。但是，当我们像函数一样调用宏时，其主体中的文本和代码范围将根据提供的参数进行评估和发出。

当调用宏时，它会获得自己的上下文。此上下文仅包含参数和任何包含的宏（请参阅下面有关包含的部分）。无法访问封闭模板的上下文。

宏需要在模板的顶层定义（因此，不能在其他宏、控制语句等内部）。

宏不能有选择地返回值。请参阅下面有关返回语句的部分。没有显式 `return` 语句的宏将始终`null`按语义返回。


## 控制流
模板语言附带 3 种基本的控制流语句。

### 如果语句
`If` 语句需要一个布尔条件来确定评估哪个文本块和代码范围。
```
{{if 1 > 2}}
   This is evaluated when someCondition is true
{{elseif 2 == 5}}
   This is evaluated when anotherCondition is true
{{else}}
   Otherwise, this will be evaluated.
{{end}}
```
```
   Otherwise, this will be evaluated.
```
当然，您可以省略`elseif` and `else`子句。

### 对于报表
`For`语句类似于`Java`的增强`for`循环，eg `for (SomeType x : someCollection)`语句。您可以迭代数组、映射值、`Iterable`实例和`Iterator`实例：
```
{{for value in array}}
   Got {{value}} from the array
{{end}}

{{for value in map}}
   Got {{value}} from the map
{{end}}

{{for value in iterable}}
   Got {{value}} from the iterable
{{end}}
```
```
context.set("array", new int[] {1, 2, 3});
Map<String, String> map = new HashMap<String, String>();
map.put("a", "Value for key a");
map.put("b", "Value for key b");
map.put("c", "Value for key c");
context.set("map", map);
Set<String> set = new HashSet<String>();
set.add("Value #1");
set.add("Value #2");
set.add("Value #3");
context.set("set", set);
result = template.render(context);
System.out.println(result);
```
```
   Got 1 from the array
   Got 2 from the array
   Got 3 from the array
   Got Value for key a from the map
   Got Value for key b from the map
   Got Value for key b from the map
   Got Value #1 from the iterable
   Got Value #2 from the iterable
   Got Value #3 from the iterable
```
如果迭代数组或映射，则可以将索引/键分配给局部变量，其作用域为 `for` 循环体：
```
{{for index, value in array}}
   Is this the first element: {{index == 0 ? "yes" : "no"}}
   Element value: {{value}}
{{end}}
```
`for` 语句不支持开箱即用的范围。但您可以在功能上发挥创意！
```
{{for value in range(0, 4)}}
   Ranged value: {{value}}
{{end}}
```
```
context.set("range", (BiFunction<Integer, Integer, Iterator<Integer>>) (from, to) -> {
   return new Iterator<Integer>() {
      int idx = from;
      public boolean hasNext () { return idx <= to; }
      public Integer next () { return idx++; }
   };
});
```
```
   Ranged value: 0
   Ranged value: 1
   Ranged value: 2
   Ranged value: 3
   Ranged value: 4
```
`while` 语句
虽然语句按预期工作：
```
{{i = 0}}
{{while i < 3}}
    {{i}}
    {{i = i + 1}}
{{end}}
```
```
0
1
2
```

### 继续并打破
您可以在上述所有循环结构中使用熟悉的`continue`and语句。`break`这些语句将作用于发出它们的最内层循环。

### 返回
`return`您可以通过在模板中的任意位置使用语句来提前退出模板。

不返回值的 `return` 语句应以“;”结束：
```
{{
    i = 5;
    if (i == 5)
        return;
    end
    // never reached
}}
```
`return` 语句还可以返回任意值：
```
{{
    return { title: "Hello world" }
}}
```
返回值的 `return` 语句不需要以;.

您还可以在宏中使用 `return` 语句让宏返回一个值。
```
{{
macro myMacro (a, b)
    return a + b
end
}}
```

## 包括
您可以在模板中包含另一个模板，如下所示：
```
{{include "path/to/template.st"}}
```
其经典应用是网站的页眉和页脚。像这样包含的模板将使用包含模板的上下文。

如果您想避免包含模板访问包含模板的上下文，您可以指定如下上下文：
```
{{include "path/to/template.st" with (var1: 1 + 2, anotherVar: "Test")}}
```
包含的模板只能访问变量`var1`和`anotherVar`。

如果你只想包含另一个模板的宏，你可以这样做：
```
{{include "path/to/template.st" as someName}}
{{someName.myMacro(1, 2)}}
```
所包含模板中包含的所有宏都可以通过变量访问`someName`。

最后，如果您想按原样包含另一个文件，而不将其解释为模板，则可以使用原始包含：
```
{{include raw "path/to/file"}}
```
当模板引擎遇到 `include` 语句时，它会使用与加载包含模板相同的模板加载器。

如果您使用基础模板类之一`TemplateLoader`来加载模板，则所有模板都将以“已编译”形式缓存。这种方式包含速度相当快。

> 注意：`include` 语句当前不允许**循环**包含模板。但您可以多次包含相同的模板。

## 范围
模板具有模板上下文形式的全局范围。模板内的所有代码范围都可以访问此范围内的变量。

`if` 条件语句、`for` 块和 `while` 块的主体创建自己的作用域。首先在该作用域内查找变量名，然后在其上方的作用域中搜索（这些语句可以嵌套）。因此，较高作用域中的变量可能会被较低作用域所掩盖。

宏创建自己的作用域，并且不会继承其定义或包含在其中的模板的作用域。宏确实可以访问同一模板中定义的其他宏以及其他模板中包含的宏。

没有上下文的包含继承了包含模板的范围。包含上下文的包含不会继承包含模板的范围。仅导入宏的包含没有任何范围。

## 并发性
基础模板`Template`和`TemplateLoader`实例是线程安全的。您可以在多个线程中并行使用它们。

`TemplateContext`不是线程安全的。始终实例化一个新上下文，并且不要重复使用旧上下文，除非您知道自己在做什么。例如，在 `Web` 服务器的请求处理程序中的多个线程之间共享单个`TemplateContext`线程是一个坏主意。1）您的线程将同时写入模板上下文，相互覆盖；2）当您的糟糕模板尝试渲染自身时，它将获取上下文中当前的任何内容，包括来自其他线程的值。

始终实例化一个新的`TemplateContext`渲染。如果你超级喜欢冒险，你可以使用 a `ThreadLocal`来缓存模板上下文实例并减少 `GC` 的压力。在这种情况下，请确保在调用 后清除上下文`Template.render()`，否则放入上下文中的任何内容都将保持活动状态并且不会被 `GC` 回收，这可能会导致严重的内存泄漏。

简而言之：
```
// setup code, e.g. when starting up your server. Hold on to the template
Template template = new ClasspathTemplateLoader().load("/path.st");

// Somewhere in your request handler:
TemplateContext context = new TemplateContext();
context.set("stuff", myStuff);
template.render(context);
```

## 国际化
不提供开箱即用的 `i18n` 支持。考虑到表现力，您可以选择您喜欢的任何风格的 `i18n` 框架，并通过对象上的函数或方法将其填充到模板中。

## 表现
将模板编译为抽象语法树，然后对其进行解释。虽然这听起来非常慢，但我们还是非常小心地确保引擎是最快的 `JVM` 模板引擎之一。

