## 简单模版引擎

`Simple template engine` 简称 `ste`，纯java实现模版引擎，无三方依赖，解析速度是freemarker的两倍。

使用说明：[使用指南](https://gitee.com/lingkang_top/ste/wikis/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97)

## 使用
```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>ste</artifactId>
    <version>1.0.0</version>
</dependency>
```

## 入门

### 一般html渲染
`test.st`
```sql
select e from UserEntity where 1=1
{{if user.id=="123"}}
    and id='123'
{{else}}
    and id!='123'
{{end}}
```
调用
```java
public class TestBase {
    @Test
    public void test() {
        TemplateLoader loader = new TemplateLoader.ClasspathTemplateLoader();
        Template template = loader.load("/test.st");
        TemplateContext context = new TemplateContext();
        UserEntity user = new UserEntity();
        context.set("user", user);
        System.out.println(template.renderTrim(context));
    }
}
```
输出：
```html
select e from UserEntity where 1=1 and id!='123'
```

### 自定义渲染
```java
    public void testTxt() {
        TemplateLoader.MapTemplateLoader loader = new TemplateLoader.MapTemplateLoader();
        loader.set("sql", "select e from UserEntity e where 1=1 {{if a>0}} and id='{{a}}'{{end}}");
        TemplateContext context = new TemplateContext();
        context.set("a", 11);
        Template template = loader.load("sql");
        String renderTrim = template.renderTrim(context);
        System.out.println(renderTrim);
    }
```
输出
```html
select e from UserEntity e where 1=1 and id='11'
```

### value
```html
// 变量
{{value}}

// map
value={{map.key}}

// 对象
昵称：{{user.nickname}}
```

### if
```html
{{if 1 > 2}}
   This is evaluated when someCondition is true
{{elseif 2 == 5}}
   This is evaluated when anotherCondition is true
{{else}}
   Otherwise, this will be evaluated.
{{end}}
```

### for
```html
// 数组
{{for value in array}}
   Got {{value}} from the array
{{end}}

// map
{{for value in map}}
    Got {{value}} from the map
{{end}}

// 迭代器
{{for value in iterable}}
    Got {{value}} from the iterable
{{end}}

// 循环索引
{{for index, value in array}}
    Is this the first element: {{index == 0 ? "yes" : "no"}}
    Element value: {{value}}
{{end}}
```

### while
```
{{i = 0}}
{{while i < 3}}
    {{i}}
    {{i = i + 1}}
{{end}}
```

### include 
```html
{{include "path/to/template.st"}}
```
